/**
 * @author Carolina Cordeiro Faria
 * RA: 1861735
 */
public class Pratica32 {
    public static double densidade(double x, double media, double desvio){
        double d = 1/(Math.sqrt(2*Math.PI)*desvio)*Math.pow(Math.E,-0.5*Math.pow((x-media)/desvio, 2));
        return d;
    }
    
    public static void main(String args[]){
        Pratica32 p32 = new Pratica32();
        System.out.println(p32.densidade(-1, 67, 3));
    }
}